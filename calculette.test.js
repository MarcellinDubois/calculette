const Calculatrice = require("./calculette")

const calc = require('./calculette.js')
describe(
    'test calcul',()=>{
        test('test 3 + 3 = 6', ()=>{
            expect(calc.add(3,3)).toBe(6)
        })
        test('test 9 - 3 = 6', ()=>{
            expect(calc.sub(9,3)).toBe(6)
        })
        test('test 9 / 3 = 3', ()=>{
            expect(calc.div(9,3)).toBe(3)
        })
        test('test 3 * 3 = 9', ()=>{
            expect(calc.mult(3,3)).toBe(9)
        })
    }
)