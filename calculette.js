        /*
        CODE VITE FAIT
        */
       class Calculatrice {
        va = "";
        symbol = "";
        constructor() {
        }

        action(value){
            if(isNaN(value) && value != "."){
                if(value == "=" || value == "CE" || value == "C"){
                    switch (value) {
                        case "=":
                            console.log(this.result(this.symbol));
                            document.getElementById("response").innerHTML = `=<span class="text-orange-500">${this.result(this.symbol)}</span>`;
                            document.getElementById("calc").innerText = "";
                            this.va = "";
                            this.symbol = "";
                            break;
                        case "CE":
                            document.getElementById("calc").innerText = "";
                            this.va = "";
                            this.symbol = "";
                            document.getElementById('response').innerHTML = "";
                            document.getElementById('calc').innerHTML = "";
                            break;
                        case "C":
                            document.getElementById("calc").innerText = "";
                            document.getElementById('response').innerHTML = "";
                            document.getElementById('calc').innerHTML = "";
                            break;
                        default:
                            break;
                    }
                }else{
                    document.getElementById('response').innerHTML = "";
                    if(this.va == ""){
                        this.va = document.getElementById("calc").innerText;
                        this.symbol = value;
                        document.getElementById("calc").innerText = "0";
                    }else{
                        this.va = this.result(this.symbol);
                        this.symbol = value;
                        document.getElementById("calc").innerText = "0";
                    }
                    document.getElementById("calc").innerText = "0";
                }
            }else{
                document.getElementById('response').innerHTML = "";
                if(document.getElementById("calc").innerText == 0){
                    document.getElementById("calc").innerText = value;
                }else{
                    document.getElementById("calc").innerText += value;
                }
            }
        }

        result(action){
            switch (action) {
                case "+":
                    return this.add(this.va*1,document.getElementById("calc").innerText*1);
                    break;
                case "-":
                    return this.sub(this.va*1,document.getElementById("calc").innerText*1);
                    break;
                case "/":
                    return this.div(this.va*1,document.getElementById("calc").innerText*1);
                    break;
                case "x":
                    return this.mult(this.va*1,document.getElementById("calc").innerText*1);
                    break;
                default:
                    break;
            }
        }

        add(a,b){
            return a+b;
        }

        sub(a,b){
            return a-b;
        }

        mult(a,b){
            return a*b;
        }

        div(a,b){
            return a/b;
        }
    }
try {
    module.exports = new Calculatrice();
} catch (error) {
        
}